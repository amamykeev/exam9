import React, {Fragment} from 'react';
import Toolbar from "../navigation/toolbar/toolbar";

import './layout.css';

const Layout = (props) => {
    return (
        <Fragment>
            <Toolbar />
            <main className="Layout-Content">
                {props.children}
            </main>
        </Fragment>
    );
};

export default Layout;