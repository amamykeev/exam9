import React, {Component} from 'react';
import {connect} from "react-redux";
import {deleteContact, getContacts} from "../../store/actions/actions";

import './contacts.css';

class Contacts extends Component {

    componentDidMount() {
        this.props.getContacts().then(() => {
            console.log(this.props.contacts)
        })
    }

    render() {
        return (
            <div className="ContactsBlock">
                <div className="Contacts">
                {this.props.contacts.map((contact) => {
                    console.log(this.props.contacts);
                    console.log(contact);
                    return (
                        <div className="Contact">
                            <div className='Image'>
                                <img src={contact.imageURL} alt=""/>
                            </div>
                            <div className="Info">
                                <h2>{contact.name}</h2>
                                <p>{contact.number}</p>
                                <p>{contact.email}</p>
                                <button onClick={this.props.deleteContact}>delete</button>
                            </div>
                        </div>
                    )
                })}
                </div>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        contacts: state.contacts
    }
};

const mapDispatchToProps = (dispatch) => {
    return {
        getContacts: () => dispatch(getContacts()),
        deleteContact: (id) => dispatch(deleteContact(id))
    }
};



export default connect(mapStateToProps, mapDispatchToProps)(Contacts);
