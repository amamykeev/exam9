import React from 'react';
import './toolbar.css';
import NavigationItems from "../navigationItems/navigationItems";

const Toolbar = () => (
    <header className="Toolbar">
        <div >
            <h1>Contacts</h1>
        </div>
        <NavigationItems/>
    </header>
);

export default Toolbar;