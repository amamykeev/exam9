import React from 'react';
import NavigationItem from "./navigationItem/navigationItem";

const NavigationItems = () => (
    <ul className="NavigationItems">
        <NavigationItem to="/pages/addNewContact" exact>Add New Contact</NavigationItem>
    </ul>
);

export default NavigationItems;