import React, { Component } from 'react';
import { postRequest } from "../../store/actions/actions";
import { connect } from "react-redux";

import './addNewContact.css';

class AddNewContact extends Component {

    state = {
        name: '',
        number: '',
        email: '',
        imageURL: '',
    };

    valueChanged = event => {
        const name = event.target.name;
        this.setState({[name]: event.target.value});
    };

    contactsHandler = () => {
        const contacts  = {...this.state};

        this.props.addContacts(contacts)
    };

    backToContacts = () => {
      this.props.history.push('/')
    };

    render() {
        return (
            <div className="AddNewContact">
                <h1>Add New Contact</h1>
                <label htmlFor="name">Name:</label>
                <input id="name" type="text" name="name"
                       onChange={this.valueChanged}
                       value={this.state.name}
                />
                <label htmlFor="number">Number:</label>
                <input id="number" type="number" name="number"
                       size="5"
                       onChange={this.valueChanged}
                       value={this.state.number}
                />
                <label htmlFor="email">Email:</label>
                <input id="email" type="email" name="email"
                       onChange={this.valueChanged}
                       value={this.state.email}
                />
                <label htmlFor="image">ImageURL:</label>
                <input id="imageURL" type="text" name="imageURL"
                       onChange={this.valueChanged}
                       value={this.state.imageURL}
                />
                <button onClick={this.backToContacts}>Back To Contact</button>
                <button onClick={this.contactsHandler}>Add To Contacts</button>
            </div>
        );
    }
}

const mapStateToProps = (state) => {
    return {
        loading: state.loading
    }
};

const mapDispatchToProps = (dispatch) => {
  return {
      addContacts: (contacts) => dispatch(postRequest(contacts))
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(AddNewContact);