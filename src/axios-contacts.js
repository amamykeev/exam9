import axios from 'axios';

const instance = axios.create({
    baseURL: 'https://exam9-jsam.firebaseio.com/'
});

export default instance;