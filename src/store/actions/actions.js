import axios from '../../axios-contacts';
export const ADD_CONTACTS = 'ADD_CONTACTS';
export const EDIT_CONTACTS = 'EDIT_CONTACTS';
export const REMOVE_CONTACTS = 'REMOVE+CONTACTS';
export const GET_CONTACTS = 'GET_CONTACTS';

export const addToContacts = () => ({type: ADD_CONTACTS});
export const removeContacts = () => ({type: REMOVE_CONTACTS});
export const getContactsSuccess = (contacts) => ({type: GET_CONTACTS, contacts});

export const getContacts = () => {
    return(dispatch) => {
        return axios.get('/contacts.json').then(response => {
            const contacts = Object.keys(response.data).map(id => {
                return {...response.data[id], id}
            });

            dispatch(getContactsSuccess(contacts));
        })
    }
};

export const postRequest = (contacts) => {
  axios.post('/contacts.json', contacts).then((dispatch) => {
      dispatch(getContacts())
  })
};

export const deleteContact = (id) => {
  return (
      axios.delete('/contacts/' + id + 'json').then(() => {
          this.props.history.push('/contacts')
      })
)
};