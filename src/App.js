import React, { Component } from 'react';
import Contacts from "./components/contacts/contacts";

import './App.css';
import {Route, Switch} from "react-router-dom";
import Layout from "./components/layout/layout";
import AddNewContact from "./components/addNewContact/addNewContact";

class App extends Component {
  render() {
    return (
        <Layout>
            <Switch>
                <Route path="/pages/addNewContact" component={AddNewContact}/>
                <Route path="/" component={Contacts}/>
            </Switch>
        </Layout>
    );
  }
}

export default App;
